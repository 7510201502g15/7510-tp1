package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/*  Responsabilidades
 *  Define los métodos necesarios para parsear operaciones binarias, heredando los de OperationParser.
 *  Esta clase plantea que puede haber otro tipo de operaciones a ser parseadas que no sean las 
 * binarias (unarias como Módulo, múltiples sumas, evaluación de sentencias IF, etc) por eso es que
 * hereda de OperationParser para que esta última no quede limitada a operaciones binarias.
 */

public abstract class BinaryOperationParser{

    public abstract String getSymbol();

    public abstract BinaryOperation createBinaryOperation(Cell targetCell, String firstOperand, String secondOperand);

    public boolean operatorIsSeparator() {
        return false;
    }

}