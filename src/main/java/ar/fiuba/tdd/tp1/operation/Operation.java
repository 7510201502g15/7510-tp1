package ar.fiuba.tdd.tp1.operation;

public abstract class Operation extends AOperation{

    String value;
    String formula;
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setFormula(String formula) {
        this.formula = formula;
    }
    
    public String getFormula() {
        return formula;
    }

}
