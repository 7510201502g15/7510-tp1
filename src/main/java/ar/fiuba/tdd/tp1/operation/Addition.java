package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/* Responsabilidades:
 * Se encarga de realizar una suma entre dos miembros.
*/

public class Addition extends BinaryOperation {

    public Addition(Cell targetCell, String left, String right) {
        super(targetCell, left, right);
    }

    @Override
    public Double calculateResult() {
        return (getTargetCell().getOperand(getLeft()) + getTargetCell().getOperand(getRight()));
    }

}