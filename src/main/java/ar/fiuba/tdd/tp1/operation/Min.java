package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class Min extends RangesFunction{

    public Min(Cell targetCell) {
        super(targetCell, false, true);
    }

    @Override
    public String processValuesRange(LinkedList<String> valuesRange) {
        return valuesRange.getFirst();
    }

}
