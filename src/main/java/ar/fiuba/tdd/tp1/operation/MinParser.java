package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class MinParser extends FunctionParser{

    public MinParser(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
        boolean variable = true;
    }

    @Override
    public String begin() {
        return "MIN(";
    }

    @Override
    public Function createFunction(Cell targetCell) {
        return new Min(targetCell);
    }
}    
