package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.CellsReferencesFinder;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;

import java.util.LinkedList;
import java.util.Stack;

public class Formula extends Operation{
    private Cell targetCell;
    private String formula;
    private LinkedList<Formula> subFormulas = new LinkedList<Formula>();
    private LinkedList<String> subFormulasResults = new LinkedList<String>();
    private LinkedList<String> operators = new LinkedList<String>();
    private LinkedList<Integer> operatorsPositions = new LinkedList<Integer>();
    private LinkedList<String> operands = new LinkedList<String>();
    private LinkedList<String> separators = new LinkedList<String>();
    private LinkedList<Integer> separatorsPositions = new LinkedList<Integer>();
    private LinkedList<String> terms = new LinkedList<String>();
    private LinkedList<FunctionParser> functionParsers;
    private LinkedList<BinaryOperationParser> operationParsers;
    boolean stringsFormula;
    static final String MINUS = "-";
    static final String PLUS = "+";

    Formula(Cell targetCell, String formula, LinkedList<FunctionParser> functionParsers, 
            LinkedList<BinaryOperationParser> operationParsers, boolean stringsFormula) {
        this.targetCell = targetCell;
        this.formula = formula;
        this.functionParsers = functionParsers;
        this.operationParsers = operationParsers;
        this.stringsFormula = stringsFormula;
        if (!hasCycles(targetCell, new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class))) {
            findOperatorsAndSeparators();
            findOperands();
            findTerms();
            execute();
        } else {
            setValue("Referencias circulares");
        }
    }


    @Override
    public void execute() {
        if (!containsFunctions()) {
            if (!containsSeparators()) {
                if (!containsOperators()) {
                    if (stringsFormula) {
                        setValue(targetCell.getString(formula));
                    } else {
                        setValue(Double.toString(targetCell.getOperand(formula)));
                    }
                } else {
                    calculateResult(operands,operators);
                }
            } else {
                setSubFormulas();
                setSubFormulasResults();
                calculateResult(subFormulasResults, separators);
            }
        } else {
            resolveFirstFunction();
            Formula formulaAux = new Formula(targetCell, formula, functionParsers, operationParsers, stringsFormula);
            setValue(formulaAux.getValue());
        }

    }

    void calculateResult(LinkedList<String> operandsOrTerms, LinkedList<String> operatorsOrSeparators) {
        LinkedList<String> opsOrTerms = (LinkedList<String>)operandsOrTerms.clone();
        LinkedList<String> optsOrSepts = (LinkedList<String>)operatorsOrSeparators.clone();
        while (opsOrTerms.size() > 1) {
            Double firstOperandOrTerm = targetCell.getOperand(opsOrTerms.pollFirst());
            Double secondOperandOrTerm = targetCell.getOperand(opsOrTerms.pollFirst());
            String operatorOrSeparator = optsOrSepts.pollFirst();
            for (BinaryOperationParser operationParser : operationParsers) {
                if (operationParser.getSymbol().equals(operatorOrSeparator)) {
                    String firstOpOrTermStr = Double.toString(firstOperandOrTerm);
                    String secondOpOrTermStr = Double.toString(secondOperandOrTerm);
                    Operation operation = operationParser.createBinaryOperation(targetCell, firstOpOrTermStr, secondOpOrTermStr);
                    operation.execute();
                    String currentResult = operation.getValue();
                    opsOrTerms.addFirst(currentResult);
                    break;
                }
            }
        }
        setValue(opsOrTerms.getFirst());
    }

    void resolveFirstFunction() {
        int pos = 0;
        int posBeginParameters = 0;
        int posBeginFunction = 0;
        Stack<FunctionParser> parsersStack = new Stack<FunctionParser>();
        while (pos < formula.length()) {
            int positionsMove = 1;
            for (FunctionParser funcParser : functionParsers) {
                if (funcParser.isFunctionBegin(formula.substring(pos))) {
                    parsersStack.push(funcParser);
                    posBeginFunction = pos;
                    positionsMove = funcParser.begin().length();
                    posBeginParameters = pos + positionsMove;
                    break;
                }
                boolean functionEndFound = executeFunctionIfEndFound(parsersStack, pos, posBeginParameters, posBeginFunction);
                if (functionEndFound) {
                    return;
                }
            }

            pos += positionsMove;
        }
    }

    public boolean executeFunctionIfEndFound(Stack<FunctionParser> parsersStack, int pos, int posBeginParam, int posBeginFunction) {
        int posBeginParameters = posBeginParam;
        if (!parsersStack.empty()) {
            FunctionParser parserFirstFunction = parsersStack.lastElement();
            if (parserFirstFunction.isFunctionEnd(formula.substring(pos))) {
                Function func = parserFirstFunction.createFunction(targetCell);
                while (posBeginParameters < pos) {
                    String param = parserFirstFunction.getParameter(formula.substring(posBeginParameters));
                    func.addParameter(param);
                    posBeginParameters += param.length() + parserFirstFunction.parameterSeparator().length();
                }
                func.execute();
                if (func.acceptsStringArguments()) {
                    stringsFormula = true;
                }
                updateFormulaWithFunctionResult(func.getValue(), posBeginFunction, pos);
                return true;
            }
        }
        return false;
    }

    public void updateFormulaWithFunctionResult(String resultFunction, int posBeginFunction, int posEndFunction) {
        replaceStringInFormula(resultFunction, posBeginFunction, posEndFunction);
    }

    public void replaceStringInFormula(String stringToPut, int posBeginReplace, int posEndReplace) {
        String precedingToFunction = formula.substring(0, posBeginReplace);
        String posteriorToFunction = formula.substring(posEndReplace + 1);
        formula = precedingToFunction + stringToPut + posteriorToFunction;
    }

    public void findOperatorsAndSeparators() {
        int posFind = 0;
        while (posFind < formula.length()) {
            int positionsMove = 1;
            for (BinaryOperationParser operationParser : operationParsers) {
                String operationSymbol = operationParser.getSymbol();
                int symbolSize = operationSymbol.length();
                int posEndCompare = posFind + symbolSize;
                if (operationSymbol.equals(formula.substring(posFind, posEndCompare))) {
                    operators.add(operationSymbol);
                    operatorsPositions.add(posFind);
                    if (operationParser.operatorIsSeparator()) {
                        separators.add(operationSymbol);
                        separatorsPositions.add(posFind);
                    }
                    positionsMove = operationSymbol.length();
                    break;
                }
            }
            posFind += positionsMove;
        }
        applyRuleOfSigns();
    }

    public void applyRuleOfSigns() {
        int firstPositionApply = firstPositionApplyRuleOfSigns();
        if (firstPositionApply != -1) {
            String firstSign = formula.substring(firstPositionApply, firstPositionApply + 1);
            String nextSign = formula.substring(firstPositionApply + 1, firstPositionApply + 2);
            String newSign = getSignRule(firstSign, nextSign);
            replaceStringInFormula(newSign, firstPositionApply, firstPositionApply + 1);
            clearLists();
            findOperatorsAndSeparators();
        }
    }

    public int firstPositionApplyRuleOfSigns() {
        for (int i = 0; i < separatorsPositions.size(); i++) {
            if (i + 1 < separatorsPositions.size()) {
                if (separatorsPositions.get(i) + 1 == separatorsPositions.get(i + 1)) {
                    return separatorsPositions.get(i);
                }
            }
        }
        return -1;
    }

    public String getSignRule(String firstSign, String nextSign) {
        if (firstSign.equals(PLUS)) {
            return nextSign;
        } else {
            if (nextSign.equals(PLUS)) {
                return MINUS;
            } else {
                return PLUS;
            }
        }
    }

    public void clearLists() {
        operatorsPositions.clear();
        operators.clear();
        separatorsPositions.clear();
        separators.clear();
        operands.clear();
        terms.clear();
    }

    public void findOperands() {
        int posIni = 0;
        for (int opPosition : operatorsPositions) {
            String operand = formula.substring(posIni, opPosition);
            operands.add(operand);
            posIni = opPosition + 1;
        }
        if (operatorsPositions.size() > 0) {
            operands.add(formula.substring(posIni));
        }
    }

    public void findTerms() {
        int posIni = 0;
        for (int sepPosition : separatorsPositions) {
            String term = formula.substring(posIni, sepPosition);
            terms.add(term);
            posIni = sepPosition + 1;
        }
        if (separatorsPositions.size() > 0) {
            terms.add(formula.substring(posIni));
        }
    }

    private void setSubFormulas() {
        for (String term : terms) {
            Formula subFormula = new Formula(targetCell, term, functionParsers, operationParsers, false);
            subFormulas.add(subFormula);
        }
    }

    private void setSubFormulasResults() {
        for (Formula subFormula : subFormulas) {
            String subResult = subFormula.getValue();
            subFormulasResults.add(subResult);
        }
    }

    public boolean containsFunctions() {
        for (FunctionParser functionParser : functionParsers) {
            if (formula.contains(functionParser.begin())) {
                return true;
            }
        }
        return false;
    }

    public boolean containsSeparators() {
        return (separators.size() > 0);
    }

    public boolean containsOperators() {
        return (operators.size() > 0);
    }

    public boolean hasCycles(Cell initialCell, DefaultDirectedGraph<String, DefaultEdge> graph) {
        CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<String, DefaultEdge>(graph);
        CellsReferencesFinder finder = new CellsReferencesFinder(initialCell);
        LinkedList<Cell> cells = finder.getCellsReferences();
        graph.addVertex(initialCell.getIdentifier());
        for (Cell cell : cells) {
            graph.addVertex(cell.getIdentifier());
            graph.addEdge(initialCell.getIdentifier(), cell.getIdentifier());
            if (cycleDetector.detectCycles() || hasCycles(cell, graph)) {
                return true;
            }
        }
        return false;
    }
}
