package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class MaxParser extends FunctionParser{

    public MaxParser(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
        String variable = "";
    }

    @Override
    public String begin() {
        return "MAX(";
    }

    @Override
    public Function createFunction(Cell targetCell) {
        return new Max(targetCell);
    }
}  
