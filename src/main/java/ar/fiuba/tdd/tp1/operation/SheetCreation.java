package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;

public class SheetCreation extends ReversibleOperation {

    private String sheetName;
    private Book targetBook;
    private int numberOfRows;
    private int numberOfColumns;
    
    public SheetCreation(Book targetBook, String sheetName, int numberOfRows, int numberOfColumns) {
        this.targetBook = targetBook;
        this.sheetName = sheetName;
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
    }

    public void execute() {
        Sheet sheet = new Sheet(targetBook, sheetName, numberOfRows, numberOfColumns);
        if (targetBook.getMaxLengthColName() < sheet.getMaxLengthColName()) {
            targetBook.setMaxLengthColName(sheet.getMaxLengthColName());
        }
        targetBook.getSheets().add(new Sheet(targetBook, sheetName, numberOfRows, numberOfColumns));
    }
    
    public void unexecute() {
        targetBook.deleteASheet(targetBook.getSheet(sheetName));
    }
}
