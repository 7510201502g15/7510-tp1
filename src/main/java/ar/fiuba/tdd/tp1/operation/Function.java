package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public abstract class Function extends Operation{
    private LinkedList<String> parameters;
    private boolean acceptsStringArguments;

    public Function(Cell targetCell, boolean acceptsStringArguments) {
        setTargetCell(targetCell);
        parameters = new LinkedList<String>();
        this.acceptsStringArguments = acceptsStringArguments;
    }

    public void addParameter(String parameter) {
        parameters.add(parameter);
    }

    public LinkedList<String> getParameters() {
        return parameters;
    }

    public boolean acceptsStringArguments() {
        return acceptsStringArguments;
    }
}
