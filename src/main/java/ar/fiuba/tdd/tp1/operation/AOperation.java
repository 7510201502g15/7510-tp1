package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/*  Responsabilidades:
 *  Establece los métodos base para cualquier operación. 
 *  Esta clase es necesaria para que OperationManager dependa de una abstracción y no de clases 
 * de operaciones concretas evitando violar el principio de Inversión de dependencia.
*/

public abstract class AOperation {

    private Cell targetCell; 
    
    public void setTargetCell(Cell cell) {
        this.targetCell = cell;
    }
    
    public Cell getTargetCell() {
        return this.targetCell;
    }
    
    public Double getOperand(String operand) {
        try {
            return Double.parseDouble(operand);
        } catch (NumberFormatException e) {
            return Double.parseDouble(targetCell.getSheet().getCell(operand).getValue());
        }
    }
    
    public abstract void execute();
    
}