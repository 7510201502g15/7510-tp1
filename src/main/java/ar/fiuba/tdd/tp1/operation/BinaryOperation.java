package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/*  Responsabilidades:
 *  Define los métodos necesarios para ejecutar operaciones binarias, heredando 
 * los de AOperation. 
 *  Esta clase plantea que puede haber otro tipo de operaciones a ser 
 * ejecutadas que no sean las binarias (unarias como Módulo, múltiples sumas, 
 * evaluación de sentencias IF, etc) por eso es que hereda de AOperation para 
 * que esta última no quede limitada a operaciones binarias.
*/

public abstract class BinaryOperation extends Operation {

    private String left;
    private String right;
    
    public abstract Double calculateResult();

    public BinaryOperation(Cell targetCell, String left, String right) {
        super.setTargetCell(targetCell);
        this.left = left;
        this.right = right;
    }

    public String getLeft() {
        return left;
    }
    
    public String getRight() {
        return right;
    }
    
    public void execute() {
        Double result = calculateResult();
        setValue(result.toString());
        this.getTargetCell().writeValue(result.toString());
    }

}