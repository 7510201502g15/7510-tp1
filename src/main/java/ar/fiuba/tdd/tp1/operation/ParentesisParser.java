package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class ParentesisParser extends FunctionParser{

    public ParentesisParser(LinkedList<FunctionParser> functionParsers,
            LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
    }

    @Override
    public String begin() {
        return "(";
    }

    @Override
    public int maxParameters() {
        return 1;
    }

    @Override
    public Function createFunction(Cell targetCell) {
        return new Parentesis(targetCell, getFunctionParsers(), getOperationParsers());
    }

    @Override
    public String parameterSeparator() {
        return ")";
    }



}
