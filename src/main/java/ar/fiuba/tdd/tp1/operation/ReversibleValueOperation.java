package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

/* Responsabilidades:
 * Se encarga de realizar una escritura a una celda.
 */
public class ReversibleValueOperation extends ReversibleOperation {
    
    public String oldValue;
    public String newValue;

    public ReversibleValueOperation(Cell targetCell, String value) {
        super.setTargetCell(targetCell);
        newValue = value;
    }

    public String getOldValue() {
        return oldValue;
    }
    
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }
    
    public String getNewValue() {
        return newValue;
    }
    
    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
    
    public void execute() {
        oldValue = this.getTargetCell().getValueNoRecalculate();
        getTargetCell().writeValue(newValue);
    }

    @Override
    public void unexecute() {
        getTargetCell().writeValue(oldValue);

    }
}