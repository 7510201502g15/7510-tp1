package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

public class AverageParser extends FunctionParser{

    public AverageParser(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        super(functionParsers, operationParsers);
        boolean variable = false;
    }

    @Override
    public String begin() {
        return "AVERAGE(";
    }

    @Override
    public Function createFunction(Cell targetCell) {
        return new Average(targetCell);
    }

}