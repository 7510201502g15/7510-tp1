package ar.fiuba.tdd.tp1.operation;

import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import java.util.LinkedList;

/* Responsabilidades:
 * Encargada de resolver operaciones sobre las celdas.
 */

public class ReversibleValueAndFormulaOperation extends ReversibleValueOperation {

    private String oldFormula;
    private String newFormula;

    public ReversibleValueAndFormulaOperation(Cell targetCell) {
        super(targetCell, null);
        super.setTargetCell(targetCell);
    }

    public String getOldFormula() {
        return oldFormula;
    }
    
    
    public String getNewFormula() {
        return newFormula;
    }
    
    public void setOldFormula(String oldFormula) {
        this.oldFormula = oldFormula;
    }
    
    public void setNewFormula(String newFormula) {
        this.newFormula = newFormula;
    }
    
    public void execute() {
        Cell target = getTargetCell();
        setOldValue(target.getValueNoRecalculate());
        LinkedList<FunctionParser> functionParsers = getTargetCell().getSheet().getBook().getFunctionParsers();
        LinkedList<BinaryOperationParser> operationParsers = getTargetCell().getSheet().getBook().getOperationParsers();

        String formulaToCalculate;
        if (this.getNewFormula() != null) {
            formulaToCalculate = this.getNewFormula();
        } else {
            formulaToCalculate = this.getTargetCell().getFormula();
        }

        Formula formula = new Formula(target, formulaToCalculate, functionParsers, operationParsers, false);

        setNewFormula(target.getFormula());
        setNewValue(target.getValueNoRecalculate());
        
        if (formula.getValue().equals("Referencias circulares")) {
            getTargetCell().setValueCircularReferences();
        } else {
            getTargetCell().writeValue(formula.getValue());
        }       
    }

    @Override
    public void unexecute() {
        getTargetCell().writeFormula(oldFormula);
        getTargetCell().writeValue(getOldValue());
    }

}