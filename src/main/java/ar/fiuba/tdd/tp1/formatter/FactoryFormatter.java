package ar.fiuba.tdd.tp1.formatter;

import java.util.Map;

public class FactoryFormatter {

    public static Formatter createFormat(Map<String, Object> parametters) {
        Formatter format = null;
        String formatName = (String) parametters.get("name");
        if (formatName.equals("Date")) {
            String value = (String) parametters.get("Date.Format");
            format = new DateFormatter(value);

        } else if (formatName.equals("Number")) {
            String value = (String) parametters.get("Number.Decimal");
            format = new NumberFormatter(Integer.parseInt(value));
        } else if (formatName.equals("Money")) {
            String value = (String) parametters.get("Number.Decimal");
            String symbol = (String) parametters.get("Money.Symbol");
            format = new MoneyFormatter(Integer.parseInt(value), symbol);
        } else {
            format = new StringFormatter();
        }
        return format;
    }
}
