package ar.fiuba.tdd.tp1.formatter;

import java.util.Map;

public class MoneyFormatter extends NumberFormatter {

    private String symbol;

    public MoneyFormatter(int decimals, String symbol) {
        super(decimals); 
        this.symbol = symbol;
        super.setName("Money");
    }
    
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String getFormattedValue(String value) {
        if (super.getFormattedValue(value).equals(super.notNumber)) {
            return "NOT MONEY";
        } else {
            return symbol + " " + super.getFormattedValue(value);
        }
    }

    @Override
    public void modifyParameters(Map<String, String> parameters) {
        parameters.put("Number.Decimal", String.valueOf(this.getDecimals()));
        parameters.put("Money.Symbol", symbol);
    }

}
