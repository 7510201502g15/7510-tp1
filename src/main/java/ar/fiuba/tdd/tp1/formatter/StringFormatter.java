package ar.fiuba.tdd.tp1.formatter;

import java.util.Map;

public class StringFormatter extends Formatter {

    public StringFormatter() {
        super.setName("String");
    }

    @Override
    public String getFormattedValue(String value) {
        return value;
    }

    @Override
    public boolean isValid(String value) {
        return true;
    }

    @Override
    public void modifyParameters(Map<String, String> parameters) {

    }
}
