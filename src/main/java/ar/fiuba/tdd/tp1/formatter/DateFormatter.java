package ar.fiuba.tdd.tp1.formatter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class DateFormatter extends Formatter {

    private String format;
    
    public DateFormatter(String format) {
        this.format = format; 
        super.setName("Date");
    }
    
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public String getFormattedValue(String value) {
        try {
            ZonedDateTime zdt = ZonedDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME);
            return zdt.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception e) {
            return "BAD DATE";
        }
    }

    @Override
    public boolean isValid(String value) {
        try {
            ZonedDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void modifyParameters(Map<String, String> parameters) {
        parameters.put("Date.Format", format);
    }

}
