package ar.fiuba.tdd.tp1.formatter;

import java.util.HashMap;
import java.util.Map;

public abstract class Formatter {
    private String name;

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public abstract String getFormattedValue(String value);

    public abstract boolean isValid(String value);

    public Map<String, String> getParamters() {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("name", getName());
        this.modifyParameters(parameters);
        return parameters;
    }

    public abstract void modifyParameters(Map<String, String> parameters);

}
