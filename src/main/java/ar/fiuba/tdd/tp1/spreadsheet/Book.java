package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.operation.AOperation;
import ar.fiuba.tdd.tp1.operation.AdditionParser;
import ar.fiuba.tdd.tp1.operation.AverageParser;
import ar.fiuba.tdd.tp1.operation.BinaryOperationParser;
import ar.fiuba.tdd.tp1.operation.ConcatenationParser;
import ar.fiuba.tdd.tp1.operation.FunctionParser;
import ar.fiuba.tdd.tp1.operation.MaxParser;
import ar.fiuba.tdd.tp1.operation.MinParser;
import ar.fiuba.tdd.tp1.operation.MultiplicationParser;
import ar.fiuba.tdd.tp1.operation.ParentesisParser;
import ar.fiuba.tdd.tp1.operation.ReversibleOperation;
import ar.fiuba.tdd.tp1.operation.SheetCreation;
import ar.fiuba.tdd.tp1.operation.SubstractionParser;


import java.util.Collection;
import java.util.LinkedList;

/* Responsabilidades: 
 * Contiene las hojas del libro y un OperationManager general de todo el libro.
 */

public class Book {

    private String name;
    protected Collection<Sheet> sheets;
    private OperationManager operationManager;
    LinkedList<FunctionParser> functionParsers;
    LinkedList<BinaryOperationParser> operationParsers;
    int maxLengthColName = 0;

    public Collection<Sheet> getSheets() {
        return sheets;
    }

    public Book() {
        this.initialize();
    }

    public Book(String bookName) {
        this.initialize();
        this.name = bookName;
    }

    private void initialize() {
        sheets = new LinkedList<Sheet>();
        operationManager = new OperationManager();
        functionParsers = new LinkedList<FunctionParser>();
        operationParsers = new LinkedList<BinaryOperationParser>();

        initializeDefaultFunctionParsers();
        initializeDefaultOperationParsers();
    }

    public Book(LinkedList<FunctionParser> functionParsers, LinkedList<BinaryOperationParser> operationParsers) {
        sheets = new LinkedList<Sheet>();
        operationManager = new OperationManager();
        this.functionParsers = functionParsers;
        this.operationParsers = operationParsers;
    }

    public void importBook(Book book) {
        this.setSheets(book.getAllSheets());
    } 
    
    void initializeDefaultFunctionParsers() {
        ParentesisParser parentesisParser = new ParentesisParser(functionParsers, operationParsers);
        functionParsers.add(parentesisParser);
        ConcatenationParser concatenationParser = new ConcatenationParser(functionParsers, operationParsers);
        functionParsers.add(concatenationParser);
        AverageParser averageParser = new AverageParser(functionParsers, operationParsers);
        functionParsers.add(averageParser);
        MinParser minParser = new MinParser(functionParsers, operationParsers);
        functionParsers.add(minParser);
        MaxParser maxParser = new MaxParser(functionParsers, operationParsers);
        functionParsers.add(maxParser);
    }

    void initializeDefaultOperationParsers() {
        AdditionParser additionParser = new AdditionParser();
        operationParsers.add(additionParser);
        SubstractionParser substractionParser = new SubstractionParser();
        operationParsers.add(substractionParser);
        MultiplicationParser multiplicationParser = new MultiplicationParser();
        operationParsers.add(multiplicationParser);        
    }

    public void addFunctionParser(FunctionParser functionParser) {
        functionParsers.add(functionParser);
    }

    public void addOperationParser(BinaryOperationParser operationParser) {
        operationParsers.add(operationParser);
    }

    public Sheet createNewSheet(String name, int numberOfRows, int numberOfColumns) {
        SheetCreation sheetCreation = new SheetCreation(this, name, numberOfRows, numberOfColumns);
        addOperation(sheetCreation);
        sheetCreation.execute();
        return getSheet(name);
    }
    
    public void deleteASheet(Sheet sheet) {
        sheets.remove(sheet);
    }

    public LinkedList<FunctionParser> getFunctionParsers() {
        return functionParsers;
    }

    public LinkedList<BinaryOperationParser> getOperationParsers() {
        return operationParsers;
    }

    public void addOperation(ReversibleOperation operation) {
        operationManager.addNewOperation(operation);
    }

    public void undo() {
        this.operationManager.undo();
    }

    public void redo() {
        this.operationManager.redo();
    }

    public Sheet getSheet(String sheetName) {
        for (Sheet sheet : sheets ) {
            if (sheet.getName().equals(sheetName)) {
                return sheet;
            }
        }
        throw new IllegalArgumentException();
    }
    
    public Collection<Sheet> getAllSheets() {
        return sheets;
    }

    public Cell getCell(String identifier) {
        String[] sheetNameAndCellId = identifier.split("!");
        if (sheetNameAndCellId.length > 2) {
            throw new IllegalArgumentException();
        }
        Sheet sheet = getSheet(sheetNameAndCellId[0]);
        return sheet.getCell(sheetNameAndCellId[1]);
    }

    public void setSheets(Collection<Sheet> sheets) {
        this.sheets = sheets;
    }
    
    public LinkedList<String> getSheetsNames() {
        LinkedList<String> sheetsNames = new LinkedList<String>();
        for (Sheet sheet : sheets) {
            sheetsNames.add(sheet.getName());
        }
        return sheetsNames;
    }
    
    public boolean isValidCellName(String cellName) {
        String[] sheetNameAndCellId = cellName.split("!");
        if (sheetNameAndCellId.length > 2) {
            return false;
        }
        return (getSheet(sheetNameAndCellId[0]).isValidCellName(sheetNameAndCellId[1]));
    }
    
    public boolean isValidRowNumber(int rowNumber) {
        for (Sheet sheet : sheets) {
            if (sheet.isValidRowNumber(rowNumber)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isValidColumnName(String colName) {
        for (Sheet sheet : sheets) {
            if (sheet.isValidColumnName(colName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isValidColumnName(String colName, String sheetName) {
        try {
            Sheet sheet = getSheet(sheetName);
            return (sheet.isValidColumnName(colName));
        } catch (Exception e) {
            return false;
        }
    }
    
    public void setMaxLengthColName(int maxLengthColName) {
        this.maxLengthColName = maxLengthColName;
    }
    
    public int getMaxLengthColName() {
        return maxLengthColName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}