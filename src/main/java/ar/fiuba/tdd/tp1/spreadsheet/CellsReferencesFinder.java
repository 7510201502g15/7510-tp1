package ar.fiuba.tdd.tp1.spreadsheet;

import java.util.Collections;
import java.util.LinkedList;

public class CellsReferencesFinder {
    
    Cell cell;
    Sheet sheetCell;
    Book bookCell;
    String formula;
    
    public CellsReferencesFinder(Cell cell) {
        this.cell = cell;
        sheetCell = cell.getSheet();
        bookCell = sheetCell.getBook();
        formula = cell.getFormula();
    }
    
    public LinkedList<String> getCellsReferencesNames() {
        LinkedList<String> cellsReferencesNames = new LinkedList<String>();
        if (formula != null) {
            int posSearch = 0;
            String firstNumericString = getFirstNumericString(posSearch);
            while (!firstNumericString.equals("") && posSearch < formula.length()) {
                int posBeginFirstNumber = formula.indexOf(firstNumericString, posSearch);
                int firstNumber = Integer.parseInt(firstNumericString);
                int posBeginSearchColName = getPosBeginSearchColName(posBeginFirstNumber);
                if (bookCell.isValidRowNumber(firstNumber)) {
                    String validColName = searchValidColName(posBeginSearchColName, posBeginFirstNumber - 1);
                    if (!validColName.equals("")) {
                        String cellName = validColName.concat(firstNumericString);
                        if (sheetCell.isValidCellName(cellName) && !cellsReferencesNames.contains(cellName)) {
                            cellsReferencesNames.add(validColName.concat(firstNumericString));
                        }
                    }
                }
                posSearch = posBeginFirstNumber + firstNumericString.length();
                firstNumericString = getFirstNumericString(posSearch);
            }
        }
        return cellsReferencesNames;
    }
    
    public LinkedList<Cell> getCellsReferences() {
        LinkedList<Cell> cellsReferences = new LinkedList<Cell>();
        LinkedList<String> cellsReferencesNames = getCellsReferencesNames();
        for (String name : cellsReferencesNames) {
            Cell cell = sheetCell.getCell(name);
            cellsReferences.add(cell);
        }
        return cellsReferences;
    }


    public String searchValidColName(int posEndSearch, int posBeginSearch) {
        int posStartPossibleColName = posBeginSearch;
        if (posEndSearch >= 0 && posBeginSearch >= 0) {
            while (posStartPossibleColName >= posEndSearch) {
                String possibleColName = formula.substring(posStartPossibleColName, posBeginSearch + 1);
                if (bookCell.isValidColumnName(possibleColName)) {
                    String sheetName = searchSheetNameFinishedIn(posStartPossibleColName - 1);
                    if (!sheetName.isEmpty()) {
                        if (bookCell.isValidColumnName(possibleColName, sheetName)) {
                            return sheetName.concat("!").concat(possibleColName);
                        }
                    } else {
                        if (bookCell.isValidColumnName(possibleColName, cell.getSheet().getName())) {
                            return possibleColName;
                        }
                    }
                }
                posStartPossibleColName --;
            }
        }
        return "";
    }

    public String searchSheetNameFinishedIn(int posEndSheetName) {
        LinkedList<String> sheetsNames = bookCell.getSheetsNames();
        Collections.sort(sheetsNames);
        if (!thereIsSheetNameSeparator(posEndSheetName)) {
            return "";
        }
        for (int i = sheetsNames.size() - 1; i >= 0; i--) {
            String sheetName = sheetsNames.get(i);
            int sheetNameSize = sheetName.length();
            int posBeginCompare = posEndSheetName - sheetNameSize;
            String sheetNameMatched = getSheetNameIfMatches(sheetName, posBeginCompare, posEndSheetName);
            if (!sheetNameMatched.isEmpty()) {
                return sheetName;
            }
        }
        return "";
    }
    
    private String getSheetNameIfMatches(String sheetName, int posBeginCompare, int posEndSheetName) {
        if (posBeginCompare >= 0) {
            String possibleSheetName = formula.substring(posBeginCompare, posEndSheetName);
            if (possibleSheetName.equals(sheetName)) {
                return sheetName;
            }
        }
        return "";
    }
    
    private boolean thereIsSheetNameSeparator(int position) {
        if (position < 0) {
            return false;
        }
        String character = formula.substring(position, position + 1);
        return (character.equals("!"));
    }

    public String getFirstNumericString(int posBegSearch) {
        int posBeginSearch = posBegSearch;
        while (posBeginSearch < formula.length()) {
            try {
                Integer.parseInt(formula.substring(posBeginSearch, posBeginSearch + 1));
                return getNumericStringStartingIn(posBeginSearch);
            } catch (Exception e) {
                posBeginSearch ++;
            }
        }
        return "";
    }

    public String getNumericStringStartingIn(int posBegin) {
        int actualPos = posBegin;
        String numericString = "";
        while (actualPos < formula.length()) {
            String actualCharacter = formula.substring(actualPos, actualPos + 1);
            try {
                Integer.parseInt(actualCharacter);
                numericString = numericString.concat(actualCharacter);
                actualPos ++;
            } catch (Exception e) {
                return numericString;
            }
        }
        return numericString;
    }
    
    private int getPosBeginSearchColName(int posBeginFirstNumber) {
        int posBeginSearchColName = posBeginFirstNumber - bookCell.getMaxLengthColName();
        if (posBeginSearchColName < 0) {
            return 0;
        }
        return posBeginSearchColName;
    }

}
