package ar.fiuba.tdd.tp1.spreadsheet;

import ar.fiuba.tdd.tp1.formatter.Formatter;
import ar.fiuba.tdd.tp1.formatter.StringFormatter;
import ar.fiuba.tdd.tp1.operation.AOperation;
import ar.fiuba.tdd.tp1.operation.ReversibleValueAndFormulaOperation;
import ar.fiuba.tdd.tp1.operation.ReversibleValueOperation;

/* Responsabilidades:
 * Contiene una fórmula y un valor actual. Ejecuta las operaciones de setear valor y calcular fórmula.
 */

public class Cell {

    private String identifier;
    private int row;
    private int column;
    private String formula;
    private String value;
    private Sheet sheet;
    private Formatter formatter;
    static final String INVALID_FORMULA = "Fórmula inválida";
    static final String CIRCULAR_REFERENCES = "Referencias circulares";
    
    public Cell(Sheet sheet, String identifier, int row, int column) {
        this.sheet = sheet;
        this.identifier = identifier;
        this.row = row;
        this.column = column;
        this.value = null;
        this.formatter = new StringFormatter();
    }

    public Cell(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public void setFormula(String formula) {        
        calculate(formula);
    }
    
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }
    
    public Formatter getFormatter() {
        return this.formatter;
    }

    public void setValue(String value) {
        if (value == null || !formatter.isValid(value)) {
            throw new IllegalArgumentException();
        }
        ReversibleValueOperation operation = new ReversibleValueOperation(this, value);
        this.sheet.getBook().addOperation(operation);
        operation.execute();
    }
    
    public void setValueInvalidFormula() {
        this.value = INVALID_FORMULA;
    }
    
    public void setValueCircularReferences() {
        this.value = CIRCULAR_REFERENCES;
    }

    public void writeValue(String value) {
        this.value = value;
    }
    
    public void writeValueAndFormula(String value, String formula) {
        this.value = value;
        this.formula = formula;
    }

    public String getFormula() {
        return formula;
    }

    public String getValue() {
        if (formula != null) {
            getValueRecalculateFormula();
        }
        if (value != null) {
            return formatter.getFormattedValue(value);
        }
        return null;
    }
    
    public String getValueRecalculateFormula() {
        try {
            calculate(formula);
        } catch (Exception e) {
            return value;
        }
        if (value.equals(INVALID_FORMULA) || value.equals(CIRCULAR_REFERENCES)) {
            return value;
        }
        return null;
    }
    
    public String getValueNoRecalculate() {
        return value;
    }
    
    public boolean hasInvalidFormula() {
        return value == INVALID_FORMULA;
    }

    public Double getOperand(String operandOrCellId) {
        Double number;
        /* Probar si se pasó directamente un número */
        try {
            number =  Double.parseDouble(operandOrCellId);
            /* Probar si se pasó el nombre de una celda con un valor válido.
             * Sino, el resultado será una IllegalArgumentException lanzada desde
             * sheet.getCell() si la celda no existe o una NullPointerException,
             * si la celda existe pero tiene valor null (se hará Double.parseDouble(null))  
             */
        } catch (NumberFormatException e) {
            number = Double.parseDouble(sheet.getCell(operandOrCellId).getValue());
        }
        return number;
    }

    public String getString(String stringValueOrCellId) {
        try {
            return (sheet.getCell(stringValueOrCellId).getValue());
        } catch (Exception e) {
            return stringValueOrCellId;
        }
    }

    public void calculate(String formula) {
        String oldFormula = this.formula;
        if (!(formula == null || formula.isEmpty())) {
            this.formula = formula.replaceAll(" ", "");
            /* Probar si como fórmula se pasó un número o una celda válida */
            try {
                setValue(Double.toString(getOperand(formula)));
            } catch (Exception e) {
                ReversibleValueAndFormulaOperation operation = new ReversibleValueAndFormulaOperation(this);
                operation.setOldFormula(oldFormula);
                this.sheet.getBook().addOperation(operation);               
                try {
                    operation.execute();
                } catch (Exception e2) {
                    setValueInvalidFormula();
                }
            }
        }
    }

    public Sheet getSheet() {
        return sheet;
    }
    
    public boolean inUse() {
        return getValue() != null;
    }

    public void writeFormula(String formula) {
        this.formula = formula;
    }

}