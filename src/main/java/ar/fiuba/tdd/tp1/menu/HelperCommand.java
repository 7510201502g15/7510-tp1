package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.IOException;

public class HelperCommand extends CommandMenu{

    @Override
    public String getDescription() {
        return "Ayuda";
    }

    @Override
    public void execute(Book book) throws IOException {
        System.out.println(
                "--------Operaciones soportadas\n\n"
                        + "** Suma y resta\n\n"
                        + "     Ejemplo:\n"
                        + "         A1 + A2 + hoja2!A2 - 10\n\n"
                        + "--------Funciones soportadas\n\n"
                        + "** Promedio de un rango\n\n"
                        + "     Ejemplo:\n"
                        + "         AVERAGE(A1:A5)\n\n"
                        + "** Máximo de un rango\n\n"
                        + "     Ejemplo:\n"
                        + "         MAX(A1:A5)\n\n"
                        + "** Mínimo de un rango\n\n"
                        + "     Ejemplo:\n"
                        + "         MIN(A1:A5)\n\n"
                        + "** Concatenación\n\n"
                        + "     Ejemplos:\n"
                        + "         CONCAT(A1:A5,B8)\n"
                        + "         CONCAT(A1:A5,B1:B8)\n"
                        + "         CONCAT(A1,B1)\n"
                        + "         CONCAT(A1,cadena)\n"
                        + "         CONCAT(cadena,A1)\n"
                        + "         CONCAT(cadena,cadena)\n\n");

    }

}
