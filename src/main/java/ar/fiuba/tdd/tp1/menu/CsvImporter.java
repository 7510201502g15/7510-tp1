package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import com.csvreader.CsvReader;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class CsvImporter  extends Importer {

    static  final char DELIMITER_CHAR = ',';
    static  final Integer POS_ID      = 0;
    static  final Integer POS_VALUE   = 1;
    static  final Integer POS_SHEET = 2;
    static  final String DIR_NAME     = "/archivos/";

    @Override
    public Book importFile(String path) throws Exception {
        Collection<Map<String, Object>> cellsSheetMap = new LinkedList<Map<String, Object>>();
        final String actuallyDir = System.getProperty("user.dir");
        path = actuallyDir + DIR_NAME + path;
        CsvReader reader = new CsvReader(path, DELIMITER_CHAR);

        String[] recordsOfData = null;
        boolean isFirst = true;
        while (reader.readRecord()) {
            if (isFirst) {
                recordsOfData = reader.getValues();
                isFirst = false;
            } else {
                this.addCellSheet(reader, cellsSheetMap, recordsOfData);

            }
        }
        reader.close();

        Book book = new Book();

        book = modifyBook(book, cellsSheetMap);
        return book;
    }

    @Override
    public void modifyCellWithFormatter(Cell cell, Map<String, Object> sheetCell) {

    }

    private void addCellSheet(CsvReader reader, Collection<Map<String,Object>> cellsSheetMap, String[] recordsOfData) {
        String[] rowOfDataList = null;
        try {
            rowOfDataList = reader.getValues();
            Map<String,Object> cellSheetMap = new HashMap<String, Object>();
            cellSheetMap.put(recordsOfData[POS_ID], rowOfDataList[POS_ID]);
            DecimalFormat decimalFormat = new DecimalFormat("0.#");
            String value = decimalFormat.format(Double.parseDouble(rowOfDataList[POS_VALUE]));
            cellSheetMap.put(recordsOfData[POS_VALUE], value);
            cellSheetMap.put(recordsOfData[POS_SHEET], rowOfDataList[POS_SHEET]);
            cellsSheetMap.add(cellSheetMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
