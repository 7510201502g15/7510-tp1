package ar.fiuba.tdd.tp1.menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

public class Helper {

    public Helper() {

    }

    public void fileCreate(String text, String fileName) {
        try {
            if (createFolderIfNotExists()) {
                String actuallyDir = System.getProperty("user.dir");
                fileName = actuallyDir + "/archivos/" + fileName;
                File file = new File(fileName);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                Writer writer = new OutputStreamWriter(fileOutputStream, "UTF-8");
                PrintWriter printWriter = new PrintWriter(writer);
                printWriter.println(text);
                printWriter.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String convertFileToString(String path) {
        String text = "";
        String actuallyDir = System.getProperty("user.dir");
        path = actuallyDir + "/archivos/" + path;
        try {
            File file = new File(path);
            FileInputStream fileInputStream = new FileInputStream(file);
            Reader reader = new InputStreamReader(fileInputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                text = text.concat(line);
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }
    
    private boolean createFolderIfNotExists() {
        File folder = new File(System.getProperty("user.dir") + "/archivos/");
        if (!folder.exists()) {
            return folder.mkdir();
        }
        return true;
    }

}