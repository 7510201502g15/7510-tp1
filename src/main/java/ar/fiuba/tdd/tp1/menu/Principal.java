package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class Principal {

    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
    private static Map<Integer, CommandMenu> commands = new HashMap<Integer, CommandMenu>();

    public static void main(String[] args) throws Exception {

        Book book = new Book();

        initializeCommands();

        while (true) {
            renderMenu();
            String option = br.readLine();
            int commandNumber = 0;
            try {
                commandNumber = Integer.parseInt(option);
            } catch (Exception ex) {
                System.out.println("Ingrese opción válida");
            }
            CommandMenu command = commands.get(commandNumber);
            if (command != null) {
                command.execute(book);
            } else {
                System.out.println("Ingrese opción válida");
            }
        }
    }

    private static void initializeCommands() {
        commands.put(1, new SheetCreator());
        commands.put(2, new CellSetter());
        commands.put(3, new BookPrinter());
        commands.put(4, new CSVBookExporter());
        commands.put(5, new JsonBookExporter());
        commands.put(6, new JsonBookImporter());
        commands.put(7, new HelperCommand());
    }

    private static void renderMenu() {
        System.out.println("Seleccione opción:");
        for (Integer commandNumber : commands.keySet()) {
            System.out.println(commandNumber + ") " + commands.get(commandNumber).getDescription());
        }
    }

}