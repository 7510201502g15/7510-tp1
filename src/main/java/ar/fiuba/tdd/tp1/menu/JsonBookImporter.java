package ar.fiuba.tdd.tp1.menu;

public class JsonBookImporter extends BookIO {

    public JsonBookImporter() {
        super("Importar desde JSON", null, new JsonImporter());
    }

}
