package ar.fiuba.tdd.tp1.menu;

public class CSVBookExporter extends BookIO {

    public CSVBookExporter() {
        super("Exportar a CSV", new CSVExporter(), null);
    }

}
