package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.formatter.FactoryFormatter;
import ar.fiuba.tdd.tp1.formatter.Formatter;
import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class JsonImporter extends Importer {

    @Override
    public Book importFile(String path) throws Exception {

        Helper writer = new Helper();
        String text = writer.convertFileToString(path);

        JSONObject jsonObject = new JSONObject(text);
        Collection<Map<String,Object>> cellsSheetMap = new LinkedList<Map<String, Object>>();
        JSONArray  sheetsCellsJson = jsonObject.getJSONArray("cells");

        for (int i = 0; i < sheetsCellsJson.length(); i++) {
            JSONObject sheetCellJson = sheetsCellsJson.getJSONObject(i);

            JSONObject formattersJson = sheetCellJson.getJSONObject("formatter");
            Map<String, Object> parameters = jsonToMap(formattersJson);
            parameters.put("name", sheetCellJson.getString("type"));
            sheetCellJson.remove("formatter");
            sheetCellJson.remove("type");

            Map<String, Object> cellSheetMap = jsonToMap(sheetCellJson);

            cellSheetMap.put("formatter", parameters);
            cellsSheetMap.add(cellSheetMap);
        }
        String bookName = jsonObject.getString("name");
        Book book = new Book(bookName);

        this.modifyBook(book, cellsSheetMap);
        return book;
    }

    @Override
    public void modifyCellWithFormatter(Cell cell, Map<String, Object> sheetCell) {
        cell.writeFormula((String) sheetCell.get("formula"));
        
        Map<String, Object> parametersMap =  (Map<String, Object>) sheetCell.get("formatter");

        Formatter formatter = FactoryFormatter.createFormat(parametersMap);
        cell.setFormatter(formatter);
    }

    private Map<String,Object> jsonToMap(JSONObject json) {
        Map<String,Object> jsonMap = new HashMap<String,Object>();

        Iterator<String> keysItr = json.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            String value = "";
            try {
                value = (String) json.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonMap.put(key, value);
        }

        return jsonMap;
    }
    
}