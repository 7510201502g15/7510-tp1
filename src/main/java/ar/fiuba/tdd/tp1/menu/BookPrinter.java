package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;
import ar.fiuba.tdd.tp1.spreadsheet.Cell;
import ar.fiuba.tdd.tp1.spreadsheet.Sheet;

import java.io.IOException;

public class BookPrinter extends CommandMenu {

    @Override
    public String getDescription() {
        return "Imprimir todo";
    }

    @Override
    public void execute(Book book) throws IOException {
        for (Sheet sheet: book.getSheets()) {
            for (int i = 0; i < sheet.getRows(); i++) {
                for (int j = 0; j < sheet.getColumns(); j++) {
                    Cell cell = sheet.getCell(i, j);
                    if (cell.inUse()) {
                        System.out.print("hoja: " + sheet.getName() + ", " + "id: " + cell.getIdentifier() + ", ");
                        System.out.println("formula: " + cell.getFormula() + ", " + "value: " + cell.getValue());
                    }
                }
            }
        }
    }

}
