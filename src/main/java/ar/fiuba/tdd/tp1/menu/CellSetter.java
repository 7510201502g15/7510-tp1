package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.Formatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatter.NumberFormatter;
import ar.fiuba.tdd.tp1.formatter.StringFormatter;
import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.IOException;

public class CellSetter extends CommandMenu {

    @Override
    public String getDescription() {
        return "Ingresar valor a celda";
    }

    @Override
    public void execute(Book book) throws IOException {
        System.out.println("Ingrese nombre hoja:");
        final String sheet = br.readLine();
        System.out.println("Ingrese nombre celda:");
        String cell = br.readLine();
        System.out.println("Ingrese formato celda (texto, fecha, numero, moneda) o Enter para no cambiarlo:");
        String formatter = br.readLine();
        System.out.println("Ingrese valor celda:");
        String cellValue = br.readLine();
        if (sheet != null && cell != null && cellValue != null) {
            book.getSheet(sheet).getCell(cell).setFormatter(getFormatterByName(formatter));
            if (cellValue.contains("=")) {
                book.getSheet(sheet).getCell(cell).setFormula(cellValue.substring(1));
            } else {
                book.getSheet(sheet).getCell(cell).setValue(cellValue);
            }
        }
    }

    private Formatter getFormatterByName(String formatter) {
        if (formatter.equals("fecha")) {
            return new DateFormatter("dd-MM-yyyy");
        } else if (formatter.equals("numero")) {
            return new NumberFormatter(2);
        } else if (formatter.equals("moneda")) {
            return new MoneyFormatter(2, "ARS");
        }
        return new StringFormatter();
    }

}
