package ar.fiuba.tdd.tp1.menu;

import ar.fiuba.tdd.tp1.spreadsheet.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public abstract class CommandMenu {

    protected static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));

    public abstract String getDescription();

    public abstract void execute(Book book) throws IOException;

}
