package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.spredsheet.SpreadSheetFacade;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CreationTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetFacade();
    }

    @Test
    public void startWithNoWorkBooks() {
        assertTrue(testDriver.workBooksNames().isEmpty());
    }

    @Test
    public void createOneWorkBook() {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertTrue(testDriver.workBooksNames().contains("tecnicas"));
    }

    @Test
    public void createMultipleWorkBooks() {
        testDriver.createNewWorkBookNamed("tecnicas 1");
        testDriver.createNewWorkBookNamed("tecnicas 2");

        assertTrue(testDriver.workBooksNames().contains("tecnicas 1"));
        assertTrue(testDriver.workBooksNames().contains("tecnicas 2"));
        assertTrue(!testDriver.workBooksNames().contains("tecnicas"));
    }

    @Test
    public void workBookStartsWithDefaultWorkSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertTrue(testDriver.workSheetNamesFor("tecnicas").contains("default"));
    }

    @Test
    public void createOneWorkSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");

        assertTrue(testDriver.workSheetNamesFor("tecnicas").contains("other"));
    }

    @Test
    public void createAdditionalWorkSheets() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "firstAdditionalWorksheet");
        testDriver.createNewWorkSheetNamed("tecnicas", "secondAdditionalWorksheet");

        assertTrue(testDriver.workSheetNamesFor("tecnicas").contains("firstAdditionalWorksheet"));
        assertTrue(testDriver.workSheetNamesFor("tecnicas").contains("secondAdditionalWorksheet"));
    }

}
